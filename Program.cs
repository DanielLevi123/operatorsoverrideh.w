﻿using System;
using System.Threading.Channels;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace HWOperatorsoverride
{
    class Program
    {
        static void Main(string[] args)
        {
            Camp c1 = new Camp(15, 30, 50, 25, 55);
            Camp c2 = new Camp(70, 80, 100, 50, 85);
            if (c1 > c2)
            {
              Console.WriteLine("c1>c2");
            }
            else
            {
                Console.WriteLine("c1<c2"); 
            }
            Camp c3 = c1 + c2;
            Console.WriteLine(c3);
        }
    }
}

