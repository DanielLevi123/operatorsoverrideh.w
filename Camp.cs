﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace HWOperatorsoverride
{
    class Camp
    {
        private readonly int id;
        public int Latitude { get; private set; }
        public int Longitude { get; private set; }
        public int NumberOfPeople { get; private set; }
        public int NumberOfTents { get; private set; }
        public int NumberOfFlashLights { get; private set; }
        private static int lastCampid = 0;

        public Camp(int latitude, int longitude, int numberOfPeople, int numberOfTents, int numberOfFlashLights)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.NumberOfPeople = numberOfPeople;
            this.NumberOfTents = numberOfTents;
            this.NumberOfFlashLights = numberOfFlashLights;
            this.id = lastCampid++;
        }
        public override string ToString()
        {
            return $"[{base.ToString()}] {Latitude} {Longitude} {NumberOfPeople} {NumberOfTents} {NumberOfFlashLights}";
        }

        public static bool operator ==(Camp c1, Camp c2)
        {
            if (ReferenceEquals(c1, null) && ReferenceEquals(c2, null))
            {
                return true;
            }

            if (ReferenceEquals(c1, null) || ReferenceEquals(c2, null))
            {
                return false;
            }
            if (c1.id == c2.id)
            {
                return true;
            }
            return false;
        }
        public static bool operator !=(Camp c1, Camp c2)
        {
            if (!(c1.id == c2.id))
            {
                return true;
            }
            return false;
        }
        public static bool operator >(Camp c1, Camp c2)
        {
            if (c1.NumberOfPeople > c2.NumberOfPeople)
            {
                return true;
            }
            return false;
        }
        public static bool operator <(Camp c1, Camp c2)
        {
            if (c1.NumberOfPeople < c2.NumberOfPeople)
            {
                return true;
            }
            return false;
        }
        public override bool Equals(Object obj)
        {
            Camp other = obj as Camp;
            return this.id == other.id;
        }
        public override int GetHashCode()
        {
            return this.id;
        }

        public static Camp operator +(Camp c1, Camp c2)
        {
            return new Camp((c1.Latitude + c2.Latitude), (c1.Longitude + c2.Longitude), (c1.NumberOfPeople + c2.NumberOfPeople),
                (c1.NumberOfTents + c2.NumberOfTents), (c1.NumberOfFlashLights + c2.NumberOfFlashLights));

        }
    }
}
